package com.fangye.base.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
/**
 * JavaScript工具类
 * @date 2021-4-19
 * @author qizenan
 */
public class JavaScriptUtil {
    public final static String EXCEPTION = "error";

    public static String evel(String str) {
        return evel(str, "JavaScript");
    }

    public static String evel(String str, String shortname) {
        String s = "";
        if (StringUtil.isBlank(str)) {
            return s;
        }
        if (StringUtil.isBlank(shortname)) {
            shortname = "JavaScript";
        }
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName(shortname);
        try {
            str = str.replace("\r", "").replace("\n", "").replace("\r\n", "");
            Object obj = engine.eval(str);
            if (null != obj) {
                s = obj.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            s = EXCEPTION;
        }
        return s;
    }

    public static void main(String[] args) {
        String s = "var sum=1+3+4+5;sum;";
        System.out.println(evel(s));
    }
}
