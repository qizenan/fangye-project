package com.fangye.base.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;

import java.io.CharArrayWriter;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * @author qizenan
 */
@Slf4j
public class TemplateUtil {

    /**
     * @Description 生成html 的临时文件
     * @date 2012-5-11 下午1:29:58
     * @Author ZhaoXiangBing
     */
    public static String createTextByTemplate(File templateFile, Map<String, Object> param) {
        try {
            if (param == null) {
                param = new HashMap<>();
            }
            Configuration configuration = new Configuration(Configuration.getVersion());
            configuration.setDefaultEncoding("utf-8");
            configuration.setDirectoryForTemplateLoading(templateFile.getParentFile());

            Template template = configuration.getTemplate(templateFile.getName(), Locale.CHINA, "UTF-8");
            CharArrayWriter out = new CharArrayWriter();
            template.process(param, out);
            return out.toString();
        } catch (Exception e) {
            log.info("templateFile={} 异常：", templateFile.getAbsolutePath(), e);
        }
        return "";
    }

}
