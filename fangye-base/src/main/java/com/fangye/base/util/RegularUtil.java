package com.fangye.base.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularUtil {
    public static void main(String[] args) {
        String extract = extract("[\\u4e00-\\u9fa5]+\\w*", "* 主键id", 0);
        System.out.println(extract);
    }

    /**
     * 根据正则表达式 抽取 content 中的数据
     *
     * @param content 内容
     * @param regex   正则表达式
     * @param index   索引：全局是0，第一个括号是1，第二个是2，以此类推
     * @return {@link String}
     */
    public static String extract(String regex, String content, Integer index) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        while (m.find()) {
            return m.group(index);
        }
        return null;
    }
}
