package com.fangye.base.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * RSA 非对称加密
 * @date 2021-4-19
 * @author qizenan
 */
@Slf4j
public class SecurityRsaUtil {
    /**
     * 公钥文件路径
     */
    private static final String publicKeyFile = "D:/datas/temp/a/public_key.pem";
    /**
     * 私钥文件路径
     */
    private static final String privateKeyFile = "D:/datas/temp/a/private_key.pem";


    /**
     * 随机生成密钥对。公钥发给别人，私钥秘密保留。
     */
    public static void genKeyPair() {
        try {
            // KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
            // 初始化密钥对生成器，密钥大小为96-1024位
            keyPairGen.initialize(1024, new SecureRandom());
            // 生成一个密钥对，保存在keyPair中
            KeyPair keyPair = keyPairGen.generateKeyPair();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();   // 得到私钥
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();  // 得到公钥

            String publicKeyString = new String(Base64.encodeBase64(publicKey.getEncoded()));
            String privateKeyString = new String(Base64.encodeBase64((privateKey.getEncoded())));

            IOUtils.write(publicKeyString, new FileOutputStream(publicKeyFile), StandardCharsets.UTF_8);
            IOUtils.write(privateKeyString, new FileOutputStream(privateKeyFile), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加密
     *
     * @param text 明文
     * @return 密文
     */
    public static String encrypt(String text) {
        try {
            String publicKeyContent = IOUtils.toString(new FileInputStream(publicKeyFile), StandardCharsets.UTF_8);
            byte[] publicKeyByte = Base64.decodeBase64(publicKeyContent);

            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(publicKeyByte);
            KeyFactory rsaKeyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = rsaKeyFactory.generatePublic(x509KeySpec);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] clear = text.getBytes(StandardCharsets.UTF_8);
            byte[] encrypted = cipher.doFinal(clear);
            return Base64.encodeBase64String(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 解密
     *
     * @param text 密文
     * @return 明文
     */
    public static String decrypt(String text) {
        try {
            String privateKeyContent = IOUtils.toString(new FileInputStream(privateKeyFile), StandardCharsets.UTF_8);

            byte[] privateKeyByte = Base64.decodeBase64(privateKeyContent);

            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKeyByte);
            KeyFactory rsaKeyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = rsaKeyFactory.generatePrivate(pkcs8EncodedKeySpec);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] encrypted = Base64.decodeBase64(text);
            byte[] decrypted = cipher.doFinal(encrypted);
            return new String(decrypted, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

    public static void main(String[] args) {
        //产生公钥私钥
//        genKeyPair();

        String password = "hello@world@123";
        System.out.println("加密前的明文=" + password);

        String encode1 = SecurityRsaUtil.encrypt(password);
        System.out.println("第一次加密后的密文=" + encode1);

        String encode2 = SecurityRsaUtil.encrypt(password);
        System.out.println("第二次加密后的密文=" + encode2);

        String decode1 = SecurityRsaUtil.decrypt(encode1);
        System.out.println("解密第一次的密文=" + decode1);

        String decode2 = SecurityRsaUtil.decrypt(encode2);
        System.out.println("解密第二次的密文=" + decode2);

    }
}

