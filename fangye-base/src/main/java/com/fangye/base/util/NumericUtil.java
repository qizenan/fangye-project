package com.fangye.base.util;

/**
 * 数字工具
 *
 * @author qizenan
 * @date 2020/03/26 18:52
 */
public class NumericUtil {

    /**
     * 从右开始，把num的二进制的第i位设置为0
     *
     * @param num 数字
     * @param i   从右到左，从0开始，第i位
     * @return int
     */
    public static int setFalse(int num, int i) {
        int mask = ~(1 << i);
        return (num & (mask));
    }


    /**
     * 从右开始，把num的二进制的第i位设置为1
     *
     * @param num 数字
     * @param i   从右到左，从0开始，第i位
     * @return int
     */
    public static int setTrue(int num, int i) {
        return (num | (1 << i));
    }

    /**
     * 获取 整数 num 的第 i 位的值,rue 表示第i位为1,否则为0
     *
     * @param num 数字
     * @param i   从右到左，从0开始，第i位
     * @return true 表示第i位为1,否则为0
     */
    public static boolean getBit(int num, int i) {
        return ((num & (1 << i)) != 0);
    }

    public static void main(String[] args) {
        boolean bit = getBit(4, 0);
        System.out.println(bit);
        bit = getBit(4, 1);
        System.out.println(bit);
        bit = getBit(4, 2);
        System.out.println(bit);
    }
}
