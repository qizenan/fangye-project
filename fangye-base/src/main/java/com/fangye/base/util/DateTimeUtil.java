package com.fangye.base.util;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 时间帮助类
 *
 * @author qizenan
 */
public class DateTimeUtil {
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 获取当天日期，格式 yyyy-MM-dd
     */
    public static String getCurrentDate() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    /**
     * 获取当天日期，格式 yyyy-MM-dd HH:mm:ss
     */
    public static String formatDateTime(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
    }

    /**
     * 获取当天日期，格式 dateFormat
     */
    public static String format(LocalDateTime localDateTime, String dateFormat) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateFormat));
    }

    /**
     * 获取指定周的每一天日期
     */
    public static List<LocalDate> getDateListOfWeek(int weekYear, int weekOfYear) {
        WeekFields weekFields = WeekFields.of(Locale.SIMPLIFIED_CHINESE);
        List<LocalDate> dateTimeList = new ArrayList<>();
        for (int i = 1; i < 8; i++) {
            dateTimeList.add(LocalDate.now().withYear(weekYear).with(weekFields.weekOfYear(), weekOfYear)
                    .with(weekFields.dayOfWeek(), i));
        }
        return dateTimeList;
    }

    /**
     * 返回日期所在年的第几周
     *
     * @param localDate 时间参数
     * @return WeekDTO
     */
    public static Week getWeekDTO(LocalDate localDate) {
        WeekFields weekFields = WeekFields.of(Locale.SIMPLIFIED_CHINESE);
        LocalDate firstDay = localDate.with(weekFields.dayOfWeek(), 1);
        LocalDate lastDay = localDate.with(weekFields.dayOfWeek(), 7);

        Week week = new Week();
        week.setYear(lastDay.getYear());
        week.setWeek(lastDay.get(weekFields.weekOfWeekBasedYear()));
        week.setFirstDay(firstDay);
        week.setLastDay(lastDay);
        return week;
    }

    @Data
    public static class Week {
        private Integer year;
        private Integer week;
        private LocalDate firstDay;
        private LocalDate lastDay;
    }
}