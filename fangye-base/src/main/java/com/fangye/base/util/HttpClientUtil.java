package com.fangye.base.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;


/**
 * @author qizenan
 */
public class HttpClientUtil {

    public static String doRequestByStream(String uri, File content, String charset)
            throws Exception {
        return doRequestByStream(uri, content, content.getName(), charset);
    }

    public static String doRequestByStream(String uri, File content,
                                           String fileName, String charset) throws Exception {
        if (StringUtil.isBlank(charset)) {
            charset = "UTF-8";
        }

        URL url = new URL(uri);
        HttpURLConnection http = (HttpURLConnection) url.openConnection();

        http.setDoOutput(true);
        http.setDoInput(true);
        http.setUseCaches(false);

        /*
          定义数据分隔线
         */
        String BOUNDARY = "---------7d4a6d158c9";

        http.setRequestMethod("POST");
        http.setRequestProperty("connection", "Keep-Alive");
        http.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
        http.setRequestProperty("Charsert", charset);
        http.setRequestProperty("Content-Type",
                "multipart/form-data; boundary=" + BOUNDARY);

        if (null != content) {
            OutputStream os = http.getOutputStream();

            StringBuilder sb = new StringBuilder();
            sb.append("--");
            sb.append(BOUNDARY);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"filename\";filename=\""
                    + fileName + "\"\r\n");
            sb.append("Content-Type:application/octet-stream\r\n\r\n");

            byte[] data = sb.toString().getBytes();
            os.write(data);
            DataInputStream in = new DataInputStream(new FileInputStream(content));
            int bytes = 0;
            byte[] bufferOut = new byte[1024];
            while ((bytes = in.read(bufferOut)) != -1) {
                os.write(bufferOut, 0, bytes);
            }
            os.write("\r\n".getBytes()); // 多个文件时，二个文件之间加入这个
            in.close();

            os.write(("\r\n--" + BOUNDARY + "--\r\n").getBytes());
            os.flush();
            os.close();
        }

        InputStream input = http.getInputStream();

        StringBuffer out = new StringBuffer();
        byte[] bufferOut = new byte[1024];
        for (int n; (n = input.read(bufferOut)) != -1; ) {
            out.append(new String(bufferOut, 0, n, charset));
        }
        return out.toString();
    }

    public static String doRequest(String uri, String content,
                                   String method, String charset) {
        return doRequest(uri, null, content, method, charset);
    }

    /**
     * 调用url
     *
     * @param uri        请求路径
     * @param headParams header 上的信息
     * @param content    参数
     * @param method     GET , POST , PUT
     * @param charset    字符集合如 UTF-8
     * @return 发放请求结果
     */
    public static String doRequest(String uri, Map<String, String> headParams, String content,
                                   String method, String charset) {
        try {
            if (StringUtil.isBlank(charset)) {
                charset = "UTF-8";
            }

            URL url = new URL(uri);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setReadTimeout(300 * 1000);
            http.setConnectTimeout(300 * 1000);
            http.setDoInput(true); // 允许输入流
            http.setDoOutput(true); // 允许输出流
            http.setUseCaches(false); // 不允许使用缓存

            http.setRequestMethod(method);
            http.setRequestProperty("Charset", charset); // 设置编码
            http.setRequestProperty("connection", "keep-alive");
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
            if (headParams != null) {
                for (String key : headParams.keySet()) {
                    http.setRequestProperty(key, headParams.get(key));
                }
            }
            http.connect();
            if (null != content && !"".equals(content.trim())) {
                OutputStream os = http.getOutputStream();
                os.write(content.getBytes(charset));// 传入参数
                os.flush();
                os.close();
            }

            InputStream input = http.getInputStream();

            StringBuffer out = new StringBuffer();
            byte[] bufferOut = new byte[1024];
            for (int n; (n = input.read(bufferOut)) != -1; ) {
                out.append(new String(bufferOut, 0, n, charset));
            }
            return out.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
