package com.fangye.base.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

/**
 * 文件工具类
 *
 * @author qizenan
 * @date 2021-4-19
 */
@Slf4j
public class FileUtil extends FileUtils {
    /**
     * UTF-8 编码
     */
    private static final String UTF_8 = "UTF-8";

    /**
     * 写文件，默认是UTF-8的格式
     *
     * @param file     文件
     * @param content  写入文件内容
     * @param encoding 文件编码格式
     */
    public static void writeFile(File file, String content, String encoding) {
        createFile(file);

        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        if (StringUtil.isBlank(encoding)) {
            encoding = UTF_8;
        }
        try {
            fos = new FileOutputStream(file);
            osw = new OutputStreamWriter(fos, encoding);
            osw.write(content);
            osw.flush();
        } catch (Exception e) {
            log.error("内容写入失败：", e);
        } finally {
            try {
                if (osw != null) {
                    osw.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 往文件追加内容，默认是UTF-8的格式
     *
     * @param file     文件
     * @param content  写入文件内容
     * @param encoding 文件编码格式
     */
    public static void addFile(File file, String content, String encoding) {
        try {
            createFile(file);

            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            long fileLength = randomAccessFile.length();
            randomAccessFile.seek(fileLength);
            randomAccessFile.write(content.getBytes(encoding));
            randomAccessFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 按行读文件
     *
     * @param file     文件
     * @param encoding 文件编码格式 默认utf-8
     */
    public static String readFile(File file, String encoding) {
        if (StringUtil.isBlank(encoding)) {
            encoding = UTF_8;
        }
        StringBuilder strContent = new StringBuilder();
        String s;
        if (file.exists()) {
            InputStreamReader read = null;
            BufferedReader br = null;
            try {
                read = new InputStreamReader(new FileInputStream(file), encoding);
                br = new BufferedReader(read);
                while ((s = br.readLine()) != null) {
                    strContent.append(s).append("\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return strContent.toString();
            } finally {
                try {
                    if (read != null) {
                        read.close();
                    }
                    if (br != null) {
                        br.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        return strContent.toString();
    }

    /**
     * 删除文件
     *
     * @param file 文件
     */
    public static void delFile(File file) {
        if (file.exists()) {
            if (!file.delete()) {
                log.info("删除文件{}失败", file.getAbsolutePath());
            }
        }
    }

    /**
     * 删除目录及目录下的所有文件
     */
    public static void allDelete(String filePath) {
        try {
            File f = new File(filePath);
            if (f.exists() && f.isDirectory()) {
                if (Objects.requireNonNull(f.listFiles()).length == 0) {
                    delFile(f);
                } else {
                    File[] delFile = f.listFiles();
                    int i = Objects.requireNonNull(f.listFiles()).length;
                    for (int j = 0; j < i; j++) {
                        if (delFile[j].isDirectory()) {
                            allDelete(delFile[j].getAbsolutePath());
                        } else {
                            delFile(delFile[j]);
                        }
                    }
                }
            } else {
                delFile(f);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 取得指定目录下的所有文件列表，包括子目录
     */
    public static ArrayList<File> getSubFiles(File baseDir) {
        ArrayList<File> ret = new ArrayList();
        try {
            File[] tmp = baseDir.listFiles();
            if (tmp != null) {
                for (File file : tmp) {
                    if (file.isFile()) {
                        ret.add(file);
                    }
                    if (file.isDirectory()) {
                        ret.addAll(getSubFiles(file));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * 复制文件。targetFile为目标文件，file为源文件
     *
     * @param targetFile     目标文件
     * @param file           源文件
     * @param targetEncoding 目标编码格式
     * @param encoding       源编码格式
     */
    public static void copyFile(File file, File targetFile, String targetEncoding, String encoding) {
        createFile(targetFile);
        String content = readFile(file, encoding);
        writeFile(targetFile, content, targetEncoding);
    }

    /**
     * 拷贝一个目录或者文件到指定路径下
     *
     * @param source 原文件
     * @param target 目标地址
     */
    public static void copy(File source, File target, String encoding, String targetEncoding) {
        File tarpath = new File(target, source.getName());
        delFile(target);
        if (source.isDirectory()) {
            tarpath.mkdir();
            File[] dir = source.listFiles();
            for (File file : dir) {
                copy(file, tarpath, encoding, targetEncoding);
            }
        } else {
            copyFile(source, target, encoding, targetEncoding);
        }

    }

    /**
     * 创建文件/文件夹
     *
     * @param file 文件
     */
    public static File createFile(File file) {
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    /**
     * 根据路径获取文件名称
     */
    public static String getFileName(String path) {
        String name = "";
        if (StringUtil.isNotBlank(path)) {
            String[] str = path.split("//");
            if (str.length == 1) {
                str = path.split("/");
            }
            if (str.length > 1) {
                name = str[str.length - 1];
            } else {
                name = path;
            }
        }
        return name;
    }

    /**
     * 根据 文件的url 获取 输入流
     *
     * @param fileUrl 文件url
     * @return 输入流
     */
    public static byte[] getFileByteArray(String fileUrl) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        try {
            //把url的图片保存的临时缓存中
            URL url = new URL(fileUrl.trim());
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setConnectTimeout(10000);
            urlCon.setReadTimeout(30000);
            InputStream inStream = urlCon.getInputStream();
            if (inStream != null) {
                int readLength;
                byte[] buf = new byte[4096];
                while (((readLength = inStream.read(buf)) != -1)) {
                    byteArrayOS.write(buf, 0, readLength);
                }
                byteArrayOS.flush();
                return byteArrayOS.toByteArray();
            }
            //把缓存中的数据保存到pdf服务器中
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据 文件的url 获取 输入流
     *
     * @param fileUrl 文件url
     * @return 输入流
     */
    public static InputStream getFileInputStream(String fileUrl) {
        return new ByteArrayInputStream(getFileByteArray(fileUrl));
    }

    /**
     * 将字符串复制到剪切板。
     */
    public static void setSysClipboardText(String writeMe) {
        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable tText = new StringSelection(writeMe);
        clip.setContents(tText, null);
    }

    /**
     * 获取项目main/resources 下的文件
     *
     * @param filePath 文件路径。如 main/resources/template 的路径是：template
     * @return 目标文件
     */
    public static String getResourceFile(String filePath) {
        String basePath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        return basePath + filePath;
    }

    public static void main(String[] args) {
        String fileName = FileUtil.getResourceFile("template/JavaTemplate");
        System.out.println(fileName);
    }
}