package com.fangye.base.util;

import java.sql.*;

/**
 * 数据库工具类
 *
 * @author qizenan
 * @date 2021-04-15
 */
public class SqlUtil {
    private static final String dbUrl = "jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=gbk&useSSL=true" +
            "&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Hongkong";
    private Connection conn;

    public static void main(String[] args) {
        SqlUtil sqlUtil = SqlUtil.getCon("192.168.0.32", "3306", "xiaozi_wms", "root", "kedu2021");
        try {
            String sql = "SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA='xiaozi_tms' AND TABLE_NAME='tms_carrier' ";
            ResultSet rs = sqlUtil.selectOne(sql);
            System.out.printf(rs.getString("TABLE_NAME"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlUtil.closeCon();
        }
    }


    /**
     * 打开 mysql链接
     *
     * @return mysql链接
     */
    public static SqlUtil getCon(String ip, String port, String dbName, String username, String password) {
        SqlUtil sqlUtil = new SqlUtil();

        String url = String.format(dbUrl, ip, port, dbName);
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            sqlUtil.conn = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlUtil;
    }

    /**
     * 关闭 mysql 链接
     */
    public void closeCon() {
        try {
            if (this.conn != null) {
                this.conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询单个对象的sql
     *
     * @param sql sql语句
     * @return ResultSet查询结果
     */
    public ResultSet selectOne(String sql) {
        try {
            PreparedStatement pst = this.conn.prepareStatement(sql);
            ResultSet resultSet = pst.executeQuery();
            if (resultSet.next()) {
                return resultSet;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 执行sql查询
     *
     * @param sql sql语句
     * @return ResultSet查询结果
     */
    public ResultSet select(String sql) {
        try {
            PreparedStatement pst = this.conn.prepareStatement(sql);
            return pst.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
