package com.fangye.base.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

/**
 * @author qizenan
 */
public class StringUtil extends StringUtils {
    public static final String UTF_8 = "UTF-8";
    public static final String ISO_8859_1 = "ISO-8859-1";
    private static final String STRS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /**
     * 随机生成字符串
     *
     * @param length 长度
     * @return 随机数
     */
    public static String generateRandStr(int length) {
        StringBuilder generateRandStr = new StringBuilder();
        Random rand = new Random();
        for (int i = 0; i < length; i++) {
            int randNum = rand.nextInt(STRS.length());
            generateRandStr.append(STRS.charAt(randNum));
        }
        return generateRandStr.toString();
    }

    /**
     * 是字符串的首字母大写 member-->Member
     *
     * @param str 字符串
     * @return 首字母大写
     */
    public static String getFirstUpperStr(String str) {
        if (isBlank(str)) {
            return "";
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * 是字符串的首字母小写 Member-->member
     *
     * @param str 字符串
     * @return 首字母小写
     */
    public static String getFirstLowerStr(String str) {
        if (isBlank(str)) {
            return "";
        }
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    /**
     * 转换字符串格式，cmember_code --> cmemberCode
     *
     * @param str 包含下划线字符串
     * @return 驼峰字符串
     */
    public static String getVoStr(String str) {
        str = str.toLowerCase();
        Pattern p = compile("_([a-z])");
        Matcher m = p.matcher(str);
        while (m.find()) {
            String tempStr = m.group(1);
            str = str.replace("_" + tempStr, tempStr.toUpperCase());
        }
        return str;
    }

    /**
     * 转换字符串格式，ResourceGlist --> resource_glist
     *
     * @param str 驼峰字符串
     * @return 包含下划线的字符串
     */
    public static String getTableStr(String str) {
        Pattern p = compile(".*([A-Z]).*");
        Matcher m = p.matcher(str);
        while (m.find()) {
            String tempStr = m.group(1);
            str = str.replace(tempStr.toUpperCase(), "_" + tempStr);
        }
        if (str.startsWith("_")) {
            str = str.substring(1);
        }
        str = str.toLowerCase();
        return str;
    }

    /**
     * 删除html特殊字符
     *
     * @param htmlStr 包含特殊字符的字符串
     * @return string
     */
    public static String delHtmlTag(String htmlStr) {
        /*定义script的正则表达式*/
        String regExScript = "<script[^>]*?>[/s/S]*?</script>";
        /*定义style的正则表达式*/
        String regExStyle = "<style[^>]*?>[/s/S]*?</style>";
        /*定义HTML标签的正则表达式*/
        String regExHtml = "<[^>]+>";

        Pattern pScript = compile(regExScript, CASE_INSENSITIVE);
        Matcher mScript = pScript.matcher(htmlStr);
        /*过滤script标签*/
        htmlStr = mScript.replaceAll("");

        Pattern pStyle = compile(regExStyle, CASE_INSENSITIVE);
        Matcher mStyle = pStyle.matcher(htmlStr);
        /*过滤style标签*/
        htmlStr = mStyle.replaceAll("");

        Pattern pHtml = compile(regExHtml, CASE_INSENSITIVE);
        Matcher mHtml = pHtml.matcher(htmlStr);
        /*过滤html标签*/
        htmlStr = mHtml.replaceAll("");

        htmlStr = htmlStr.replaceAll("&nbsp;", " ");

        return htmlStr.trim();
    }

    /**
     * 判断一个字符串是否为url
     *
     * @param str String 字符串
     * @return boolean 是否为url
     **/
    public static boolean isURL(String str) {
        str = str.toLowerCase();
        String regex = "^((https|http|ftp|rtsp|mms)?://)"  //https、http、ftp、rtsp、mms
                + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@
                + "(([0-9]{1,3}\\.){3}[0-9]{1,3}" // IP形式的URL- 例如：199.194.52.184
                + "|" // 允许IP和DOMAIN（域名）
                + "([0-9a-z_!~*'()-]+\\.)*" // 域名- www.
                + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\\." // 二级域名
                + "[a-z]{2,6})" // first level domain- .com or .museum
                + "(:[0-9]{1,5})?" // 端口号最大为65535,5位数
                + "((/?)|" // a slash isn't required if there is no file name
                + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
        return str.matches(regex);
    }
}
