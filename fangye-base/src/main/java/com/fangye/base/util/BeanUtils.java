package com.fangye.base.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.net.URLClassLoader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 反射的Utils函数集合.扩展自Apache Commons BeanUtils
 *
 * @author qizenan
 */
@Slf4j
public class BeanUtils extends org.apache.commons.beanutils.BeanUtils {

    private BeanUtils() {
    }

    /**
     * 根据方法名获取 Method
     *
     * @param clazz      目标对象
     * @param methodName 方法名
     * @return Method
     */
    public static Method getMethod(Class clazz, String methodName) {
        for (Class superClass = clazz; superClass != Object.class; superClass = superClass.getSuperclass()) {
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                if (method.getName().equals(methodName)) {
                    return method;
                }
            }
        }
        return null;
    }

    /**
     * 根据目标对象，属性名得到属性Field
     *
     * @param object       目标对象
     * @param propertyName 属性名
     */

    public static Field getDeclaredField(Object object, String propertyName) throws NoSuchFieldException {
        return getDeclaredField(object.getClass(), propertyName);
    }

    /**
     * 根据目标类，属性名得到属性Field
     *
     * @param clazz        目标类
     * @param propertyName 属性名
     */
    public static Field getDeclaredField(Class clazz, String propertyName) throws NoSuchFieldException {
        for (Class superClass = clazz; superClass != Object.class; superClass = superClass.getSuperclass()) {
            try {
                return superClass.getDeclaredField(propertyName);
            } catch (NoSuchFieldException e) {
                log.info("NoSuchFieldException：", e);
            }
        }
        throw new NoSuchFieldException("No such field: " + clazz.getName() + '.' + propertyName);
    }

    /**
     * 获取属性值
     *
     * @param object       目标对象
     * @param propertyName 属性名
     */
    public static Object getObjProperty(Object object, String propertyName) {
        Field field = null;
        try {
            field = getDeclaredField(object, propertyName);
        } catch (NoSuchFieldException e1) {
            log.info("NoSuchFieldException:", e1);
        }
        Object result = null;
        if (null != field) {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            try {
                result = field.get(object);
            } catch (Exception e) {
                log.info("Exception:", e);
            }
            field.setAccessible(accessible);
        }
        return result;
    }

    /**
     * 获取属性值(包括Map)
     *
     * @param object       目标对象
     * @param propertyName 属性名
     */
    public static Object getObjValue(Object object, String propertyName) {
        Object result = null;
        try {
            if (object instanceof Map) {
                result = ((Map) object).get(propertyName);
            } else {
                result = getObjProperty(object, propertyName);
            }
        } catch (Exception e) {
            log.info("Exception:", e);
        }
        return result;
    }

    /**
     * 给对象的属性设置值
     *
     * @param object       目标对象
     * @param propertyName 属性名
     * @param newValue     属性值
     */
    public static void setObjProperty(Object object, String propertyName, Object newValue) {
        try {
            Field field = getDeclaredField(object, propertyName);
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            field.set(object, newValue);
            field.setAccessible(accessible);
        } catch (Exception e) {
            log.info("{}.{} = {} . Exception:", object.getClass(), propertyName, newValue, e);
        }
    }

    /**
     * 给对象的属性设置值（包括Map）
     *
     * @param object       目标对象
     * @param propertyName 属性名
     * @param newValue     属性值
     */
    @SuppressWarnings("unchecked")
    public static void setObjValue(Object object, String propertyName, Object newValue) {
        if (null == object || StringUtils.isBlank(propertyName)) {
            {
                return;
            }
        }
        String[] s = propertyName.split("\\.");
        for (int i = 0; i < s.length - 1; i++) {
            object = getObjValue(object, s[i]);
        }
        try {
            if (object instanceof Map) {
                ((Map) object).put(propertyName, newValue);
            } else {
                setObjProperty(object, propertyName, newValue);
            }
        } catch (Exception e) {
            log.info("Exception:", e);
        }
    }

    /**
     * 调用对象的私有方法
     *
     * @param object     目标对象
     * @param methodName 方法名
     * @param params     参数集合
     */
    @SuppressWarnings("unchecked")
    public static Object invokePrivateMethod(Object object, String methodName, Object... params)
            throws NoSuchMethodException {
        Class[] types = new Class[params.length];
        for (int i = 0; i < params.length; i++) {
            types[i] = params[i].getClass();
        }

        Class clazz = object.getClass();
        Method method = null;
        for (Class superClass = clazz; superClass != Object.class; superClass = superClass.getSuperclass()) {
            try {
                method = superClass.getDeclaredMethod(methodName, types);
                break;
            } catch (NoSuchMethodException e) {
                log.info("NoSuchMethodException:", e);
            }
        }

        if (method == null) {
            throw new NoSuchMethodException("No Such Method:" + clazz.getSimpleName() + methodName);
        }

        boolean accessible = method.isAccessible();
        method.setAccessible(true);
        Object result = null;
        try {
            result = method.invoke(object, params);
        } catch (Exception e) {
            log.info("Exception:", e);
        }
        method.setAccessible(accessible);
        return result;
    }


    /**
     * 只拷贝超类里的数据
     *
     * @param dest 目标对象
     * @param orig 源对象
     */
    @SuppressWarnings("unchecked")
    public static void copySupperPropertys(Object dest, Object orig) throws Exception {
        if (null != dest && null != orig) {
            Object value;
            if (orig instanceof Map) {
                for (String key : ((Map<String, Object>) orig).keySet()) {
                    value = getObjValue(orig, key);
                    setObjValue(dest, key, value);
                }
            } else {
                Field[] field = getObjSupperProperty(orig);
                if (null != field) {
                    for (Field field1 : field) {
                        value = getObjValue(orig, field1.getName());
                        setObjValue(dest, field1.getName(), value);
                    }
                }
            }
        } else {
            throw new Exception("参数为空");
        }
    }

    /**
     * 只拷贝超类里的数据
     *
     * @param dest 目标对象
     * @param orig 源对象
     */
    public static void copyObjValue(Object dest, Object orig, String propertyName) throws Exception {
        if (null != dest && null != orig) {
            Object value = getObjValue(orig, propertyName);
            setObjValue(dest, propertyName, value);
        } else {
            throw new Exception("参数为空");
        }
    }

    /**
     * 只拷贝超类里的数据
     *
     * @param dest 目标对象
     * @param orig 源对象
     */
    @SuppressWarnings("unchecked")
    public static void copyAllPropertys(Object dest, Object orig) throws Exception {
        if (null != dest && null != orig) {
            Object value;
            if (orig instanceof Map) {
                for (String key : ((Map<String, Object>) orig).keySet()) {
                    value = getObjValue(orig, key);
                    setObjValue(dest, key, value);
                }
            } else {
                Field[] field = getObjAllProperty(orig);
                if (null != field) {
                    for (Field field1 : field) {
                        value = getObjValue(orig, field1.getName());
                        setObjValue(dest, field1.getName(), value);
                    }
                }
            }
        } else {
            throw new Exception("参数为空");
        }
    }

    /**
     * 拷贝的数据(不包括继承的)
     *
     * @param dest 目标对象
     * @param orig 源对象
     */
    @SuppressWarnings("unchecked")
    public static void copyOwnPropertys(Object dest, Object orig) throws Exception {
        if (null != dest && null != orig) {
            Object value;
            if (orig instanceof Map) {
                for (String key : ((Map<String, Object>) orig).keySet()) {
                    value = getObjValue(orig, key);
                    setObjValue(dest, key, value);
                }
            } else {
                Field[] field = getObjOwnProperty(orig);
                if (null != field) {
                    for (Field field1 : field) {
                        value = getObjValue(orig, field1.getName());
                        setObjValue(dest, field1.getName(), value);
                    }
                }
            }
        } else {
            throw new Exception("参数为空");
        }
    }

    /**
     * 把orig拷贝给dest，不包括null信息
     *
     * @param dest 目标对象
     * @param orig 源对象
     */
    public static void copyPropertiesNotNull(Object dest, Object orig) throws Exception {
        if (null != dest && null != orig) {
            Field[] origFields = getObjAllProperty(orig);
            for (Field origField : origFields) {
                Object value = getObjValue(orig, origField.getName());
                if (null != value) {
                    setObjValue(dest, origField.getName(), value);
                }
            }

        } else {
            throw new Exception("参数为空");
        }
    }

    /**
     * 获取对象的属性(不包括继承的)
     *
     * @param obj 对象
     * @return Field[]
     */
    public static Field[] getObjOwnProperty(Object obj) {
        Class c = obj.getClass();
        return c.getDeclaredFields();
    }

    /**
     * 获取对象祖先的属性
     *
     * @param obj 对象
     * @return Field[]
     */
    public static Field[] getObjSupperProperty(Object obj) {
        Class c = obj.getClass();
        Class supper = c.getSuperclass();
        List<Field> list = new ArrayList<>();
        if (null != supper) {
            for (Class superClass = supper; superClass != Object.class; superClass = superClass.getSuperclass()) {
                Field[] fieldchild = superClass.getDeclaredFields();
                if (null != fieldchild) {
                    Collections.addAll(list, fieldchild);
                }
            }
        }
        Field[] field = new Field[list.size()];
        field = list.toArray(field);
        return field;
    }

    /**
     * 获取对象所有的属性(包括继承的)
     *
     * @param obj 对象
     * @return Field[]
     */
    public static Field[] getObjAllProperty(Object obj) {
        List<Field> list = new ArrayList<>();
        for (Class superClass = obj.getClass(); superClass != Object.class; superClass = superClass.getSuperclass()) {
            Field[] fieldchild = superClass.getDeclaredFields();
            if (null != fieldchild) {
                Collections.addAll(list, fieldchild);
            }
        }
        Field[] field = new Field[list.size()];
        field = list.toArray(field);
        return field;
    }

    /**
     * 获取方法的参数名
     *
     * @param method 方法
     * @return 参数名集合
     */
    public static List<String> getMethodParamNames(Method method) {
        LocalVariableTableParameterNameDiscoverer discover = new LocalVariableTableParameterNameDiscoverer();
        String[] parameterNames = discover.getParameterNames(method);
        if (parameterNames == null || parameterNames.length == 0) {
            return Arrays.stream(method.getParameters()).map(Parameter::getName).collect(Collectors.toList());
        } else {
            return Arrays.stream(parameterNames).collect(Collectors.toList());
        }
    }

    /**
     * 从jarPath获取classFullNam的class类
     *
     * @param jarPath       jar的路径。如：file:///D:/qzn/Workspace/IdeaShuke20201231/app-client-group/build/libs/app-client-group.jar
     * @param classFullName class的全称。如：com.chiyue.qiye.client.group.controller.ReportClientGroupLearningController
     * @return Class
     */
    public static Class<?> getClassByJarClass(String jarPath, String classFullName) {
        try {
            URL[] urls = new URL[]{new URL(jarPath)};
            URLClassLoader loader = new URLClassLoader(urls);
            return loader.loadClass(classFullName);
        } catch (Exception e) {
            log.info("jarPath={},classFullNam={} 异常：", jarPath, classFullName, e);
        }
        return null;
    }

    /**
     * 把字符串自动转化类型
     *
     * @param value 字符串值
     * @param clazz 类型
     * @param <T>   类型
     * @return 转化后的值
     */
    @SuppressWarnings("unchecked")
    public static <T> T convertValue(String value, Class<T> clazz) {
        String type = clazz.getSimpleName();
        if (StringUtils.equalsAny(type, "int", "Integer")) {
            return (T) new Integer(value);
        } else if (StringUtils.equalsAny(type, "long", "Long")) {
            return (T) new Long(value);
        } else if (StringUtils.equalsAny(type, "double", "Double")) {
            return (T) new Double(value);
        } else if (StringUtils.equalsAny(type, "float", "Float")) {
            return (T) new Float(value);
        } else if (StringUtils.equalsAny(type, "Date")) {
            return (T) new Date();
        } else if (StringUtils.equalsAny(type, "LocalDate")) {
            return (T) LocalDate.parse(value);
        } else if (StringUtils.equalsAny(type, "LocalDateTime")) {
            return (T) LocalDateTime.parse(value);
        } else if (StringUtils.equalsAny(type, "boolean", "Boolean")) {
            return (T) (StringUtils.equalsAny(value, "true", "1") ? Boolean.TRUE : Boolean.FALSE);
        }
        return (T) value;
    }

    /**
     * 根据类名获取类
     *
     * @param classSimpleName 类的简易名称
     * @return class
     */
    public static Class convertClass(String classSimpleName) {
        if (StringUtils.equalsAny(classSimpleName, "int", "Integer")) {
            return Integer.class;
        } else if (StringUtils.equalsAny(classSimpleName, "long", "Long")) {
            return Long.class;
        } else if (StringUtils.equalsAny(classSimpleName, "double", "Double")) {
            return Double.class;
        } else if (StringUtils.equalsAny(classSimpleName, "float", "Float")) {
            return Float.class;
        } else if (StringUtils.equalsAny(classSimpleName, "Date")) {
            return Date.class;
        } else if (StringUtils.equalsAny(classSimpleName, "LocalDate")) {
            return LocalDate.class;
        } else if (StringUtils.equalsAny(classSimpleName, "LocalDateTime")) {
            return LocalDateTime.class;
        } else if (StringUtils.equalsAny(classSimpleName, "boolean", "Boolean")) {
            return Boolean.class;
        } else if (StringUtils.equalsAny(classSimpleName, "char", "Char", "String")) {
            return String.class;
        } else {
            throw new RuntimeException(String.format("%s 错误", classSimpleName));
        }
    }

    /**
     * 判断是否是基本类型
     *
     * @param classSimpleName class的基本类型
     * @return true 是基本类型，false 不是基本类型
     */
    public static boolean checkBaseClass(String classSimpleName) {
        return StringUtils.equalsAny(classSimpleName,
                "int", "Integer", "long", "Long", "double", "Double", "float", "Float",
                "String", "LocalDate", "LocalDateTime", "Date", "boolean", "Boolean", "char", "Char");
    }

    /**
     * 判断是否是复杂类型
     *
     * @param classSimpleName class的基本类型
     * @return true 是复杂类型，false 不是复杂类型
     */
    public static boolean checkObjectClass(String classSimpleName) {
        if (checkBaseClass(classSimpleName)) {
            return false;
        }
        return !StringUtils.equalsAny(classSimpleName, "List", "Map", "String[]");
    }
}

