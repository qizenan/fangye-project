package com.fangye.base.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 转换 工具
 *
 * @author zenan.qi
 * @date 2021/07/23
 */
public class ConvertUtil {
    /**
     * inputStream 转 outputStream
     *
     * @param in 输入流
     * @return {@link ByteArrayOutputStream}
     */
    public static ByteArrayOutputStream parse(final InputStream in) {
        try {
            final ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
            int ch;
            while ((ch = in.read()) != -1) {
                swapStream.write(ch);
            }
            return swapStream;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("inputStream 转 outputStream 失败");
        }
    }

    /**
     * outputStream转inputStream
     *
     * @param out 输出流
     * @return {@link ByteArrayInputStream}
     */
    public static ByteArrayInputStream parse(final OutputStream out) {
        ByteArrayOutputStream baos = (ByteArrayOutputStream) out;
        final ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
        return swapStream;
    }

    /**
     * inputStream转String
     *
     * @param in 输入流
     * @return {@link String}
     * @throws Exception 异常
     */
    public static String parseString(final InputStream in) {
        try {
            final ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
            int ch;
            while ((ch = in.read()) != -1) {
                swapStream.write(ch);
            }
            return swapStream.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("inputStream转String失败");
        }
    }

    /**
     * OutputStream 转String
     *
     * @param out 输出流
     * @return {@link String}
     */
    public static String parseString(final OutputStream out) {
        ByteArrayOutputStream baos = (ByteArrayOutputStream) out;
        final ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
        return swapStream.toString();
    }

    /**
     * String转inputStream
     */
    public static ByteArrayInputStream parseInputStream(final String in) {
        return new ByteArrayInputStream(in.getBytes());
    }

    /**
     * String 转outputStream
     */
    public static ByteArrayOutputStream parseOutputStream(final String in) {
        return parse(parseInputStream(in));
    }
}
