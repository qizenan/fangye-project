package com.fangye.base.util;


import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

/**
 * java调用系统默认浏览器打开链接
 * @date 2021-4-19
 * @author qizenan
 */
@Slf4j
public class OpenExplorerUtil {
    /**
     * 用浏览器打开 url
     *
     * @param url 网址
     */
    public static void browse(String url) {
        try {
            // 获取操作系统的名字
            String osName = System.getProperty("os.name", "");
            if (osName.startsWith("Mac OS")) {
                // 苹果的打开方式
                Class fileMgr = Class.forName("com.apple.eio.FileManager");
                Method openURL = fileMgr.getDeclaredMethod("openURL",
                        String.class);
                openURL.invoke(null, url);
            } else if (osName.startsWith("Windows")) {
                // windows的打开方式。
                Runtime.getRuntime().exec(
                        "rundll32 url.dll,FileProtocolHandler " + url);
            } else {
                // Unix or Linux的打开方式
                String[] browsers = {"firefox", "opera", "konqueror", "epiphany",
                        "mozilla", "netscape"};
                for (String browser : browsers) {
                    // 执行代码，在brower有值后跳出，
                    // 这里是如果进程创建成功了，==0是表示正常结束。
                    int browserExec = Runtime.getRuntime().exec(new String[]{"which", browser}).waitFor();
                    if (browserExec == 0) {
                        Runtime.getRuntime().exec(new String[]{browser, url});
                        break;
                    }

                }
            }
        } catch (Exception e) {
            log.info("打开浏览器异常：", e);
        }
    }

    // 主方法 测试类
    public static void main(String[] args) {
        String url = "http://iteye.blog.163.com/";

        OpenExplorerUtil.browse(url);
    }

}

