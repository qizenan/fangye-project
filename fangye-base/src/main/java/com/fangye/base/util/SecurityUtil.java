package com.fangye.base.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * MD5、SHA1都是通过对数据进行计算，来生成一个校验值，该校验值用来校验数据的完整性。具有不可逆，压缩性
 *
 * @author qizenan
 * @date 2021-4-19
 */
@Slf4j
public class SecurityUtil {
    /**
     * 盐，用于混交
     */
    private static final String slat = "&%5123***&&%%$$#@";

    private SecurityUtil() {
    }

    /**
     * MD5
     *
     * @param data 数据
     */
    public static String getMD5(String data) {
        String base = data + "/" + slat;
        return DigestUtils.md5Hex(base);
    }

    /**
     * CRC
     *
     * @param data 数据
     */
    public static String getSHA1(String data) {
        String base = data + "/" + slat;
        return DigestUtils.sha1Hex(base);
    }

    public static void main(String[] args) {
        String password = "hello@world@123123";
        System.out.println("原数据=" + password);
        String md5 = SecurityUtil.getMD5(password);
        System.out.println("md5加密后=" + md5 + " ,位数=" + md5.length());
        String sha1 = SecurityUtil.getSHA1(password);
        System.out.println("sha1加密后=" + sha1 + " ,位数=" + sha1.length());
    }
}

