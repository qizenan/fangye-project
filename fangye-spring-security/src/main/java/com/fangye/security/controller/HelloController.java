package com.fangye.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * hello world
 *
 * @author qizenan
 * @date 2021-4-22
 */
@RestController
public class HelloController {
    @GetMapping("hello")
    public String hello() {
        return "hello spring security";
    }
}
