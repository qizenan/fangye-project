package com.fangye.security.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * 用户登录功能
 *
 * @author qizenan
 * @date 2021-4-26
 */
@Configuration
public class MyUserDetailService implements UserDetailsService {
    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String password = passwordEncoder.encode("123456");
        System.out.println(String.format("password= %s ", password));
        Collection authorities = AuthorityUtils.commaSeparatedStringToAuthorityList("admin,agent");
        return new User(username, password, authorities);
    }
}
